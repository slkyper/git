#     math functions - matematikos funkcijos:
def even_or_odd(number):  # even - lyginis , odd - nelyginis skaicius
    if number % 2 == 0:     # % - dalybos liekana lygi 0 , tada skaicius even.
        return "Even"
    else:
        return "Odd"

def positive_sum(arr):    # arr -> An array is a data structure that contains a group of elements.
    positive = 0               # By using a "while" or "for" loop.
    for e in arr:         # Example: a = positive_sum([1,7,13]) Output: 21
        if e > 0:         # positive - sudedami tik teigiami skaiciai.
            positive = positive + e
    return positive

def opposite(number):  # opposite -> priesingas skaicius.
    return -number      # Example: 12 , -3 Output: -12 , 3

def multiply(a, b):     # multiplay -> Daugyba.
    return a * b

def remove_char(s):     # panaikina pirma ir paskutine raide is stringo.
    return s[1:-1]

def find_multiples(integer, limit):     # integer -> daugyba is saves , nustatant limita.
    return list(range(integer, limit + 1, integer))     # Example 3, 9 Output: 3, 6, 9

def capitalize_word(word):      # pakeicia pirmo zodzio pirma raide i didziaja.
    return word[0].upper() + word[1:]   # return word.title()

def weather_info(fahrenheit):
  celsius = (fahrenheit - 32) * (5.0 / 9)  # celsius = formule
  return str(celsius)  #  + " is freezing temperature" if celsius <= 0 else str(celsius) + " is above freezing temperature"

def area_or_perimeter(length, width):    # skaiciuoja perimetra
    return length ** 2 if length == width else (length + width) * 2      #  **2 -> kvadratu; **3 -> kelimas kubu.

def parse_float(string):       # String skaicius -> Float , galima ir int.
    try:
        return float(string)   # Example: "23457.567" -> Output: 23457.567
    except:
        return None

def solution(long, short):     # short long short
    x = short + long + short if len(long) > len(short) else long + short + long
    return x

def repeat_str(repeat, string):
    return repeat * string

def speed_m_per_s(s):     # Input: 100km/h Output: 27,77.. m/s greitis.
    x = (float(s)/0.036)/100        # greicio formule.
    return float(x)

def check_exam(correct_test, test):
    k = 0                                # a place in memory where we shall store the points
    for x in range(len(correct_test)):  # lets check every answer in the correct_test
        if test[x] != '':                # and if the answer is not empty
            if correct_test[x] == test[x]:
                k += 4                    # we will give +4 point for the correct answer
            else:
                k -= 1                         # and -1 for any other (incorrect) answers.
    if k < 1:
        return 0                    # if we have 0 ot negative points we should return just zero
    return k

def is_divisible(num, x, y):        # num -> parodo ar x ir y dalinasi.
    return True if num % x == num % y == 0 else False       # % -> kad liekana lygi 0

def quarter_of(month):  # Example : month 12 -> return 4 ketvertis; month 3 -> 1 ketvertis
    return (month + 2) // 3   #  // -> return int. ( sveika skaiciu )

def derive(number1, number2):
    a = number1 * number2
    b = number2 - 1
    txt = "{}x^{}"
    return txt.format(a, b)

def expression_matter(a, b, c):     # Visi imanomi veiksmai  kurie parodo didziausia suma.
    x = a + b + c
    y = (a + b) * c
    z = a * (b+c)                   # Veiksmu galima prideti daug.
    p = a**2
    o = b**2
    i = c**2
    q = a * b * c
    sum = max(x,y,z,q,p,o,i)
    return sum      # return max(a*b*c, a+b+c, (a+b)*c, a*(b+c))

def first_non_consecutive(a_list):
    i = a_list[0]       # i = skaiciu sarasas.
    for e in a_list:    # e -> tikrina musu sarase
        if e != i:      # jeigu e nelygu i tai grazinti e.
            return e
        i += 1          # jeigu e lygu i , tai eiti pridedant viena skaiciu ir tikrinti toliau.
    return a_list       # grazina tapati lista , jei neranda ,kazko netinkamo.
